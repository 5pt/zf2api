<?php

namespace ApiTest\Controller;

use Api\Service\WriterService;
use ApiTest\Bootstrap;
use Api\Controller\ApiController;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use Zend\Mvc\Router\Http\TreeRouteStack as HttpRouter;
use PHPUnit_Framework_TestCase;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class ApiControllerTest extends PHPUnit_Framework_TestCase
{
    protected $traceError = true;
    protected $controller;
    protected $request;
    protected $response;
    protected $routeMatch;
    protected $event;

    protected function setUp()
    {
        $serviceManager = Bootstrap::getServiceManager();
        $this->controller = new ApiController();
        $this->request    = new Request();
        $this->routeMatch = new RouteMatch(array('controller' => 'api'));
        $this->event      = new MvcEvent();
        $config = $serviceManager->get('Config');
        $routerConfig = isset($config['router']) ? $config['router'] : array();
        $router = HttpRouter::factory($routerConfig);
        $this->event->setRouter($router);
        $this->event->setRouteMatch($this->routeMatch);
        $this->controller->setEvent($this->event);
        $this->controller->setServiceLocator($serviceManager);
    }

    public function testMethodCanBeAccessed()
    {
        $this->request->setMethod('post');
        $this->request->getPost()->set('string', 'test');

        $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testCreateResponseBody()
    {
        $this->request->setMethod('post');
        $this->request->getPost()->set('string', 'abc');

        $result = $this->controller->dispatch($this->request);

        $expected = array(
            'result' => 'cba'
        );

        $this->assertEquals($expected, $result);
    }

    public function testGetMethodNotAllowed()
    {
        $this->request->setMethod('get');

        $result = $this->controller->dispatch($this->request);

        $expected = array(
            'content' => 'Method Not Allowed'
        );

        $this->assertEquals($expected, $result);
    }

    public function testUpdateMethodNotAllowed()
    {
        $this->request->setMethod('put');

        $result = $this->controller->dispatch($this->request);

        $expected = array(
            'content' => 'Method Not Allowed'
        );

        $this->assertEquals($expected, $result);
    }

    public function testDeleteMethodNotAllowed()
    {
        $this->request->setMethod('delete');

        $result = $this->controller->dispatch($this->request);

        $expected = array(
            'content' => 'Method Not Allowed'
        );

        $this->assertEquals($expected, $result);
    }

    public function test404()
    {
        $this->routeMatch->setParam('action', 'action-that-doesnt-exist');
        $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();
        $this->assertEquals(404, $response->getStatusCode());
    }

    public function test405()
    {
        $this->routeMatch->setParam('controller', 'controller-that-doesnt-exist');
        $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();
        $this->assertEquals(405, $response->getStatusCode());
    }
}