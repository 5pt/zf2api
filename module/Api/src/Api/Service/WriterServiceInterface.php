<?php

namespace Api\Service;

/**
 * Interface WriterServiceInterface
 * @package Api\Service
 */
interface WriterServiceInterface
{
    /**
     * @param $data
     * @return mixed
     */
    public function save($data);
}