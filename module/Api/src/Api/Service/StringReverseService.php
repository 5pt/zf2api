<?php

namespace Api\Service;

/**
 * Class StringReverseService
 * @package Api\Service
 */
class StringReverseService implements StringReverseServiceInterface
{
    /**
     * @param $data
     */
    public function strrev(&$data)
    {
        $data['reversed_string'] = strrev($data['string']);
    }
}