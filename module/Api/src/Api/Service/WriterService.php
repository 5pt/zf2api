<?php

namespace Api\Service;

use Doctrine\ORM\EntityManager;
use Api\Entity\Log;

/**
 * Class WriterService
 * @package Api\Service
 */
class WriterService implements WriterServiceInterface
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * WriterService constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        // EntityManager instance
        $this->em = $entityManager;
    }

    /**
     * Save passed data to DB
     * @param $data
     */
    public function save($data)
    {
        // Create Log Entity instance
        $log = new Log();

        // Set parameters
        $log->setQuery($data['string']);
        $log->setResult($data['reversed_string']);
        $log->setCreated(new \DateTime());

        // Prepare object to be saved
        $this->em->persist($log);

        // Flush everything to DB
        $this->em->flush();
    }
}