<?php

namespace Api\Service;

interface StringReverseServiceInterface
{
    /**
     * @param $data
     * @return mixed
     */
    public function strrev(&$data);
}