<?php

namespace Api\Controller;

use Api\Service\StringReverseServiceInterface;
use Api\Service\WriterServiceInterface;
use Zend\Mvc\Controller\AbstractRestfulController;

/**
 * Class ApiController
 * @package Api\Controller
 */
class ApiController extends AbstractRestfulController
{
    /**
     * @var WriterServiceInterface
     */
    protected $writerService;

    /**
     * @var StringReverseServiceInterface
     */
    protected $strRevService;

    /**
     * ApiController constructor.
     * @param WriterServiceInterface $writerService
     * @param StringReverseServiceInterface $stringReverseService
     */
    public function __construct(WriterServiceInterface $writerService, StringReverseServiceInterface $stringReverseService)
    {
        $this->writerService = $writerService;
        $this->strRevService = $stringReverseService;
    }

    /**
     * @param mixed $data
     * @return mixed
     * @throws \Exception
     */
    public function create($data)
    {
        // If nothing is sent to our API or
        // If there are no `string` variable in sent data
        // throw exteption
        if (empty($data) || !isset($data['string'])) {
            throw new \Exception('Invalid parameters. Method accepts only one parameter: `string`');
        }

        // Send data variable by reference
        $this->strRevService->strrev($data);

        // Save data to DB
        $this->writerService->save($data);

        // Put reversed string to $response variable
        $response['result'] = strrev($data['string']);

        return $response;
    }
}