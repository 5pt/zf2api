<?php

namespace Api\PostProcessor;

/**
 * Class Json
 * @package Api\PostProcessor
 */
class Json extends AbstractPostProcessor
{
    // Override parent`s method
    public function process()
	{
	    // Encode _vars to JSON format
		$result = \Zend\Json\Encoder::encode($this->_vars);

        // Set content to return
		$this->_response->setContent($result);

        // Set reponse HEADERS
		$headers = $this->_response->getHeaders();
		$headers->addHeaderLine('Content-Type', 'application/json');
		$this->_response->setHeaders($headers);
	}
}
