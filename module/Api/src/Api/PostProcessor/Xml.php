<?php

namespace Api\PostProcessor;

/**
 * Class Xml
 * @package Api\PostProcessor
 */
class Xml extends AbstractPostProcessor
{
    // Override parent`s method
    public function process()
    {
        // Create XML Element
        $xml = new \SimpleXMLElement("<?xml version=\"1.0\"?><response></response>");

        // Create XML from JSON
        $this->createXmlNode($this->_vars, $xml);
        $result = $xml->asXML();

        // Set content to return
    	$this->_response->setContent($result);

        // Set response HEADERS
    	$headers = $this->_response->getHeaders();
    	$headers->addHeaderLine('Content-Type', 'application/xml');
    	$this->_response->setHeaders($headers);
    }

    /**
     * @param $result
     * @param $xml
     */
    protected function createXmlNode($result, &$xml)
    {
        foreach($result as $key => $value) {
            if (is_array($value)) {
                if (!is_numeric($key)){
                  $subnode = $xml->addChild("$key");
                  $this->createXmlNode($value, $subnode);
                } else {
                    $this->createXmlNode($value, $xml);
                }
            } else {
                $xml->addChild("$key", "$value");
            }
        }
    }
}
