<?php

namespace Api\PostProcessor;

/**
 * Class AbstractPostProcessor
 * @package Api\PostProcessor
 */
abstract class AbstractPostProcessor
{
    /**
     * @var null
     */
    protected $_vars = null;

    /**
     * @var null|\Zend\Http\Response
     */
    protected $_response = null;

    /**
     * AbstractPostProcessor constructor.
     * @param \Zend\Http\Response $response
     * @param null $vars
     */
    public function __construct(\Zend\Http\Response $response, $vars = null)
    {
        $this->_vars = $vars;
        $this->_response = $response;
    }

    /**
     * @return null|\Zend\Http\Response
     */
    public function getResponse()
    {
        return $this->_response;
    }

    /**
     * @abstract
     */
    abstract public function process();
}
