<?php

namespace Api\Factory;

use Api\Controller\ApiController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class ApiControllerFactory
 * @package Api\Factory
 */
class ApiControllerFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return ApiController
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        // Get real service factory
        $realServiceLocator = $serviceLocator->getServiceLocator();

        // Get WriterService instance
        $writerService = $realServiceLocator->get('Api\Service\WriterServiceInterface');

        // Get StringReverseService instance
        $strRevService = $realServiceLocator->get('Api\Service\StringReverseServiceInterface');

        // Return new ApiController instance with WriterService & StringReverseService
        return new ApiController($writerService, $strRevService);
    }
}