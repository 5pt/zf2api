<?php

namespace Api\Factory;

use Api\Service\WriterService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class WriterServiceFactory
 * @package Api\Factory
 */
class WriterServiceFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return WriterService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        // Get EntityManager
        $entityManager = $serviceLocator->get('Doctrine\ORM\EntityManager');

        // Return WriterService instance with EntityManager
        return new WriterService($entityManager);
    }
}