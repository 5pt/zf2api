<?php

namespace Api;

return [
    // Router
    'router' => [
        'routes' => [
            'restful' => [
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => [
                    'route' => '/:controller[.:formatter]',
                    'constraints' => [
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'formatter' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ],
                ],
            ],
        ],
    ],
    // Doctrine
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity'],
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver',
                ],
            ],
        ],
    ],
    // Service Manager
    'service_manager' => [
        'invokables' => [
            'Api\Service\StringReverseServiceInterface' => 'Api\Service\StringReverseService',
        ],
        'factories' => [
            'Api\Service\WriterServiceInterface' => 'Api\Factory\WriterServiceFactory',
        ],
    ],
    // Controllers
    'controllers' => [
        'factories' => [
            'api' => 'Api\Factory\ApiControllerFactory',
        ],
    ],
    // Dependency Injection
    'di' => [
        'instance' => [
            'alias' => [
                'json-pp' => 'Api\PostProcessor\Json',
                'xml-pp' => 'Api\PostProcessor\Xml',
            ],
        ],
    ],
    // Errors
    'errors' => [
        'post_processor' => 'json-pp',
        'show_exceptions' => [
            'message' => true,
            'trace' => false,
        ],
    ],
];